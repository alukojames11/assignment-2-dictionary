section .text
%include "words.inc"

global _start
extern exit
extern print_string
extern print_newline
extern string_length
extern read_word
extern find_word

%define buffer_size 255

%include "colon.inc"

section .data
	word_buffer: times buffer_size db 0x00
	not_working: db "not_working", 10, 0
	begin: db"Welcome, write key: ", 0
	here: db "answer = ", 0


%include "colon.inc"

section .text

_start: 
	mov rdi, begin
	call print_string

	mov rdi, word_buffer
	mov rsi, buffer_size
	call read_word
	mov rdi, word_buffer
	mov rsi, firstly
	call find_word
	test rdi, rdi
	jnz .working
	mov rdi, not_working
	call print_string
	mov rax, 1
	call exit

.working:
	push rdi
	mov rdi, here
	call print_string
	pop rdi
	call print_string
	xor rax, rax
	call exit


